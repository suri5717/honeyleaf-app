'use strict';

/**
 * @ngdoc function
 * @name app.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the suriApp
 */
angular.module('app')
    .controller('DashboardCtrl', ['$scope', '$http', '$rootScope', 'loginService', function($scope, $http, $rootScope, loginService) {
        //Check if the user is logged in
        if (!loginService.isLoggedIn()) {
            // redirect user to login page
            window.location = '#/';
        }

        $scope.user = loginService.getUserData();
        $scope.showList = 'leads';

        $scope.logout = function() {
            loginService.logout();
        };

        $scope.showUsers = function(force) {
            force = force || false;
            $scope.showList = 'users';
            // Fetch the employees under the current user if not already fetched
            if (!$scope.usersTable || force) {
                $http.get($rootScope.apiHost + '/api/v1/users')
                    .then(function(response) {
                        var data = response.data;
                        if (data.success) {
                            if ($scope.usersTable){
                              $scope.usersTable.destroy();
                            }
                            $scope.usersTable = $('#user-table').DataTable({
                                "data": data.data.users,
                                "columns": [{
                                    "data": "name"
                                }, {
                                    "data": "email"
                                }, {
                                    "data": "mobile"
                                }, {
                                    "data": "type"
                                }, {
                                    "data": "address"
                                }, {
                                    "data": "createdBy.email"
                                }, {
                                    "data": "createdAt"
                                }]
                            });
                        } else {
                            if(data.isLoggedIn === false){
                              localStorage.removeItem('user');
                              localStorage.removeItem('loggedIn');
                              window.location = '#/';
                            }
                        }
                    });
            }
        };

        $scope.showLeads = function(force) {
            force = force || false;
            $scope.showList = 'leads';
            // Fetch the leads if not already fetched
            if (!$scope.leadsTable || force) {
                $http.get($rootScope.apiHost + '/api/v1/leads')
                     .then(function(response) {
                        var data = response.data;
                        if(data.isLoggedIn === false){
                          localStorage.removeItem('user');
                          localStorage.removeItem('loggedIn');
                          window.location = '#/';
                        }
                        if (data.success) {
                          if ($scope.leadsTable) {
                            $scope.leadsTable.destroy();
                          }
                          $scope.leadsTable = $('#lead-table').DataTable({
                              "data": data.data.leads,
                              "columns": [{
                                  "data": "name"
                              }, {
                                  "data": "leadNo"
                              }, {
                                  "data": "email"
                              }, {
                                  "data": "mobile"
                              }, {
                                  "data": "status"
                              }, {
                                  "data": "source"
                              }, {
                                  "data": "locality"
                              }, {
                                  "data": "assignedTo.email"
                              }, {
                                  "data": "createdAt"
                              }]
                          });
                        } else {
                            console.log(data.statusMessage);
                        }
                    });
            }
        };

        $scope.showLeads();

        $scope.addLead = function() {
            $http.post($rootScope.apiHost + '/api/v1/leads', {
                name: $scope.newLeadName,
                email: $scope.newLeadEmail,
                mobile: $scope.newLeadMobile,
                source: $scope.newLeadSource,
                locality: $scope.newLeadLocality,
                assignedTo: $scope.newLeadAssignTo
            }).then(function(response) {
                var data = response.data;
                $scope.createLeadMsg = data.statusMessage;
                if (data.success) {
                    var lead = data.data.lead;
                    $scope.leadsTable.row.add([
                        lead.name,
                        lead.leadNo,
                        lead.email,
                        lead.mobile,
                        lead.status,
                        lead.source,
                        lead.locality,
                        lead.assignedTo.name,
                        lead.createdAt
                    ]).draw(false);
                } else {
                  if(data.isLoggedIn === false){
                    localStorage.removeItem('user');
                    localStorage.removeItem('loggedIn');
                    window.location = '#/';
                  }
                }
            });
        };

        $scope.addUser = function() {
            $http.post($rootScope.apiHost + '/api/v1/users', {
                name: $scope.newUserName,
                email: $scope.newUserEmail,
                password: $scope.newUserPassword,
                user_type: $scope.newUserType,
                manager_email: $scope.newUserManager
            }).then(function(response) {
                var data = response.data;
                $scope.createUserMsg = data.statusMessage;
                if (data.success) {
                    // add user to data table
                    $scope.showUsers(true);
                } else {
                  if(data.isLoggedIn === false){
                    localStorage.removeItem('user');
                    localStorage.removeItem('loggedIn');
                    window.location = '#/';
                  }
                }
            });
        };
    }]);
