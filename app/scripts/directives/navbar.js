'use strict';

/**
 * @ngdoc directive
 * @name app.directive:navBar
 * @description
 * # navBar
 */
angular.module('app')
  .directive('navBar', function () {
    return {
      templateUrl: '../../views/navbar.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
      }
    };
  });
