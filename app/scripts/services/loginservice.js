'use strict';

/**
 * @ngdoc service
 * @name app.loginService
 * @description
 * # loginService
 * Service in the app.
 */
angular.module('app')
  .service('loginService', function ($http, $rootScope) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    return {
    	isLoggedIn: function() {
    		return localStorage.loggedIn === 'true';
    	},
    	logout: function () {
    		localStorage.loggedIn = false;
            localStorage.removeItem('user');
    		$http.post($rootScope.apiHost + '/api/v1/auth/logout')
    		.then(function(response){
    			var data = response.data;
                if (data.success || data.isLoggedIn === false) {
                    window.location = '#/';
                }else {
    				console.log('Error while logging out the user - ', data.statusMessage);
                }
    		});
    	},
    	getUserData: function () {
    		return localStorage.user ? JSON.parse(localStorage.user) : null;
    	}
    };
  });
