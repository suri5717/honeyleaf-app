'use strict';

/**
 * @ngdoc function
 * @name app.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the app
 */
angular.module('app')
    .controller('ProfileCtrl', ['$scope', '$http', '$rootScope', 'loginService', function($scope, $http, $rootScope, loginService) {
        if (!loginService.isLoggedIn()) {
            // redirect user to login page
            window.location = '#/';
        }

        $scope.user = loginService.getUserData();
        $scope.currentPassword = 'admin';
        $scope.newPassword = 'new';
        $scope.confirmPassword = 'new';

        $scope.updateProfile = function() {
            $http.put($rootScope.apiHost + '/api/v1/users/profile', $scope.user)
                .then(function(response) {
                    var data = response.data;
                    if (data.success) {
                        $scope.flashMsg = 'Profile is updated successfully';
                        localStorage.user = JSON.stringify(data.data.user);
                    } else if(data.isLoggedIn != false){
                        $scope.flashMsg = data.statusMessage;
                    } else {
                        localStorage.removeItem('user');
                        localStorage.removeItem('loggedIn');
                        window.location = '#/';
                    }
                });
        };

        $scope.changePassword = function() {
            if(!$scope.currentPassword || !$scope.newPassword || !$scope.confirmPassword){
                $scope.flashMsg = 'All fields are mandatory';
            }else if($scope.newPassword !== $scope.confirmPassword){
                $scope.flashMsg = 'Passwords not matched';
            }else {
                $scope.flashMsg = '';
                var data = {
                    current_password: $scope.currentPassword,
                    password: $scope.newPassword,
                    confirm_password: $scope.confirmPassword
                };
                $http.put($rootScope.apiHost + '/api/v1/users/change_password', data)
                    .then(function(response) {
                    var data = response.data;
                    if (data.success) {
                        $scope.flashMsg = 'Password is changed successfully';
                    } else if(data.isLoggedIn != false){
                        $scope.flashMsg = data.statusMessage;
                    } else {
                          localStorage.removeItem('user');
                          localStorage.removeItem('loggedIn');
                          window.location = '#/';
                    }
                });
            }
        };
    }]);
