'use strict';

/**
 * @ngdoc function
 * @name app.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('LoginCtrl', ['$scope', '$http', '$rootScope', 'loginService', function ($scope, $http, $rootScope, loginService) {
    //Check of user is logged in
    $scope.user = {};
    $scope.error = '';
    $scope.resetPassword = false;
    // $scope.user.email ="admin@honeycomb.com";
    // $scope.user.password ="test";

    if (loginService.isLoggedIn()) {
        // redirect user to login page
        window.location = '#/dashboard';
    }

    $scope.signin = function () {
    	$http.post($rootScope.apiHost + '/api/v1/auth/login', {
            email: $scope.user.email,
            password: $scope.user.password
        })
        .then(function (response) {
        	var data = response.data;
        	if(data.error) {
        		$scope.error = data.statusMessage;
        		loginService.loggedIn = false;
        	} else if (data.statusCode === 200){
        		// store user data in localstorage
        		localStorage.loggedIn = true;
        		localStorage.user = JSON.stringify(data.data.user);
        		window.location = '#/dashboard';
        	}
        });
    };
    $scope.forgotPassword = function(){
        if(!$scope.user.email){
            $scope.error = 'Please enter email address';
        }else{
            $scope.error = '';
            $http.post($rootScope.apiHost + '/api/v1/auth/forgot_password', {
                email: $scope.user.email
            })
            .then(function (response) {
                var data = response.data;
                if(data.error) {
                    $scope.error = data.statusMessage;
                    $scope.user.token = '';
                } else if (data.statusCode === 200){
                    // store user data in localstorage
                    $scope.user.token = data.token;
                    $scope.resetPassword = true;
                }
            });
        }
    };
    $scope.updatePassword = function(){
        if(!$scope.user.password || !$scope.user.cPassword){
            $scope.error = 'Please enter both passwords';
        }else if($scope.user.password !== $scope.user.cPassword){
            $scope.error = 'Passwords not matched';
        }else if(!$scope.user.token){
            $scope.error = 'Something went wrong';
            $scope.resetPassword = false;
        }else {
            $scope.error = '';
            $http.post($rootScope.apiHost + '/api/v1/auth/reset_password', {
                password: $scope.user.password,
                token: $scope.user.token
            })
            .then(function (response) {
                var data = response.data;
                if(data.error) {
                    $scope.error = data.statusMessage;
                } else if (data.statusCode === 200){
                    localStorage.loggedIn = true;
                    localStorage.user = JSON.stringify(data.data.user);
                    window.location = '#/dashboard';
                }
            });
        }
    };
  }]);
