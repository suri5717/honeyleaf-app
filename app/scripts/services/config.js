'use strict';

/**
 * @ngdoc service
 * @name app.config
 * @description
 * # config
 * Service in the app.
 */
angular.module('app')
  .service('config', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
