'use strict';

/**
 * @ngdoc function
 * @name app.controller:UsermgntCtrl
 * @description
 * # UsermgntCtrl
 * Controller of the suriApp
 */
angular.module('app')
    .controller('UsermgntCtrl', function($scope,userService) {
        userService.getAllRes().then(function(response) {
            $scope.data = response.data;
            console.log($scope.data);
        });
    });
